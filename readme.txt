==========================================================================================================

This repository contains the implementation of the following paper:

A. Chaudhury, F, Boudon and C. Godin, "3D Plant Phenotyping: All You Need is Labelled Point Cloud Data",
in Proceedings of Computer Vision Problems in Plant Phenotyping (CVPPP), in Conjunction with European 
Conference on Computer Vision (ECCV), Glasgow, Scotland, August 2020.

----------------------------------------------------------------------------------------------------------

The platform which is used in the implementation is the L-Py framework:

F. Boudon, C. Pradal, T. Cokelaer, P. Prusinkiewicz and C. Godin, "L-Py: an L-system simulation framework
for modeling plant architecture development based on a dynamic language", Frontiers in Plant Science, 2012.

L-Py installation guide: https://github.com/fredboudon/lpy

A user guide of L-Py can be found here: https://lpy.readthedocs.io/en/latest/

The code is tested in Python 2.7 and 3.7 version of L-Py

----------------------------------------------------------------------------------------------------------


The repository contains 3 directories: Arabidopsis, Lilac_2_labels, and Lilac_multilabel

The "Arabidopsis" directory contains the code to generate the Arabidopsis model as in the paper (needs to
run the code with the "Textures" folder in the same directory, which is used to generate texture in the
virtual model just for nice visualization purpose).
The directory "Lilac_2_labels" implements a synthetic Lilac plant with 2 labels (internode and flower),
and "Lilac_multilabel" directory is the same as "Lilac_2_labels" except where it is shown that the number
of labels can be customized according to the need and we assign each flower with a different label.

All the directories contain some generated point cloud models in (x,y,z,r,g,b) format, where the r,g,b
triplet associated with each point denotes its label (can be viewed in software like 'CloudCompare'). 


----------------------------------------------------------------------------------------------------------

General instruction: Basic use

All the models have 2 main files: a .lpy extension file for L-Py dependent parts, and a utilities.py file
for pure Python dependent parts of the code.

Open the .lpy file in L-Py editor. Click "run". Depending on the number of points to be sampled, it might
take long time. For example, to generate 100k points on the Arabidopsis model, it takes about 1.5 hours.
The number of points can be controlled by changing the variable 'totalNumberOfResampledPts' in the End()
function.

The program will still run if the End() function is commented, without which the point cloud will not be
generated and only the virtual model will be shown. So in order to see the virtual model before generating
the time consuming point sampling, it's better to comment the End() function. However, each instance of the
program execution generates a different model due to the inherent stochasticity in the procedural model. In
order to generate the same model at different instances of program execution, a fixed seed value should be
used. Some example seeds are shown at the beginning of the program in the Arabidopsis model, which are used
to generate the provided sample models. 
 

The basic point sampler generates labelled point cloud data in (x,y,z,r,g,b) format in a .xyz text file in
the same directory. It is possible to generate only the point cloud without label, and/or only the corresponding
labels in a text file. See the "write2File()" examples in the End() function.

In order to add plain noise to the data, uncomment the part of the code in the End() function as mentioned
in "Add plain noise" part. The 'sigma' of the noise can be controlled in the "convertToNoisyData()" function
in the "utilities.py" file.

Note: As we perform insideness testing after sampling the points, the desired number of points will no longer
be equal to 'totalNumberOfResampledPts', because some points remaining inside the primitives will be discarded.
In order to get the exact number of points as 'totalNumberOfResampledPts', the insideness testing should be
performed inside sampleRandomPoints() function (which is pretty straightforward) in "utilities.py", so that
for every generated point we can check if it is inside or not, and accept or discard it accordingly. However,
we do it on purpose after point generation because it's easier to debug & check if the insideness testing works
for any random model.

The block of code commented as "for debugging purpose" just saves the labelled points before insideness
testing. Uncomment the block if a new model is implemented and it is needed to test if the insideness checking
is working properly or not.
----------------------------------------------------------------------------------------------------------

General instruction: Advance use

The code block in "For advance usage" contains some of the advanced functionalities like adding device specific
noise and the distance factor on point density. 

In order to perform device specific noise operation, the corresponding block of code should be uncommented in
the End() function (and the basic point sampler should be performed before that, since the device specific noise
will be applied on the generated points). The customized noise can be implemented in the "convertToSensorNoisyData()"
function in the "utilities.py" file, where we have provided a basic functionality. The camera position can be
controlled according to the need of the application by changing the 'cameraPosition' variable in the same function.
If no camera position is suuplied, then the default position which is used to visualize the virtual model will be
considered. Also, the maximum amount of noise to be modelled is controlled by 'maxSigma' variable.

In order to add the distance effect, uncomment the corresponding block in the End() function. Any customization
according to the need of the application should be performed in the "selectRandomTriangleWithDistanceEffect()"
function in the "utilities.py" file. 


----------------------------------------------------------------------------------------------------------

Useful facts:

In order to implement a new virtual model, start with the commented End() function in the lpy file, which prints
the lstring id's of the model. This is helpful in checking all the lstrings and the function arguments before
performing the point sampling, since these are used in assigning labels to the points.

In order to add the distance effect to a model, the number of points should be significantly higher enough than
the number of triangles in the model. Otherwise the part of the model having tiny triangles will have higher density. 
Ideally the triangles should be chosen according to the density of neighbouring triangles, but that is not 
implemented in the current framework.

----------------------------------------------------------------------------------------------------------



